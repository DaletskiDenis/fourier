import pylab
from matplotlib import pyplot
from numpy import arange
from math import pi, sin, cos
import time

def integral(left, right, function1, function2 = lambda x: 1, coefficient = 1, accuracy = 1e-2):   
    """ calculates integral
    left - left bound
    right - right bound
    function1 - function under integral
    function2 - second function (sin of cos, multiplicated by first function)
    coefficient - coefficient of second function ( sin ( x * coef) ) """

    x = arange(left, right, accuracy)
    y = sum(function1(i) * function2(i * coefficient) for i in x)
    return y * accuracy
      

def furie(function, accuracy):
    """ approximates the function by Fourier series
    accuacy - number of An and Bn, accuracy of approximation """

    a0 = 1 / pi * integral(-pi, pi, function)  # a0

    aList = [(1 / pi * integral(-pi, pi, function, cos, i + 1))
    for i in range(0, accuracy)]   # An for n in [1, accuracy]
    
    bList = [1 / pi * integral(-pi, pi, function, sin, i + 1) 
    for i in range(0, accuracy)]   # Bn for n in [1, accuracy]
    
    return lambda x: a0 / 2 + sum(aList[i] * cos((i+1) * x) + bList[i] * sin((i+1) * x) 
    for i in range(0, accuracy))
    """ returns function that calculates the value of approximated function """
