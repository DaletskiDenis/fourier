from furie import integral, furie 
from numpy import arange
from matplotlib import pyplot
from math import pi

def function(x):  # user defined function
    return x
    
    
x = arange(-pi, pi, 0.01) # list of x args
x1 = arange(-pi, pi, 0.1)
y = [function(i) for i in x] # list of y args (function(x))
y1 = [furie(function, 0)(i) for i in x1]

pyplot.ion()
pyplot.show()

for j in range(8):
    y1 = [furie(function, j)(i) for i in x1]
    pyplot.cla()
    pyplot.plot(x, y, x1, y1)
    pyplot.draw()

time.sleep(5)
